import urllib.request
import shutil
import os
import tarfile

# Download the file from `url` and save it locally under `file_name`:
links = {}
links['bioc'] = "http://2013.bionlp-st.org/supporting-resources/bioc.tar.gz?attredirects=0"

outf = {}
outf['bioc'] = "../resrc/targz/bioc.tar.gz"

outd = {}
outd['bioc'] = "../resrc/bioc/"
def dlFile(url, file_name):
    os.makedirs(os.path.dirname(file_name), exist_ok=True)
    with urllib.request.urlopen(url) as response, open(file_name, 'wb') as out_file:
        shutil.copyfileobj(response, out_file)

def unzipFile(file_name, out_name):
    if (file_name.endswith("tar.gz")):
        tar = tarfile.open(file_name, "r:gz")
        tar.extractall(out_name)
        tar.close()
    elif (file_name.endswith("tar")):
        tar = tarfile.open(file_name, "r:")
        tar.extractall(out_name)
        tar.close()

def biocCopy():
    suffixes = ['.xml','_sentence.xml','_pos+lemma.xml']
    for suffix in suffixes:
        src = outd['bioc']+'bioc/data/bionlp-st-2013_cg_train_devel_texts'+suffix
        dst = outd['bioc']+'texts'+suffix
        shutil.copyfile(src,dst)
    dtds = ['collection.dtd','sentence_annotation.dtd','sentence.dtd']
    for dtd in dtds:
        src = outd['bioc']+'bioc/data/'+dtd
        dst = outd['bioc']+dtd
        shutil.copyfile(src,dst)

# dlFile("http://2013.bionlp-st.org/tasks/BioNLP-ST_2013_CG_training_data.tar.gz?attredirects=0" , "../resrc/targz/train.tar.gz")
# unzipFile("../resrc/targz/train.tar.gz", "../resrc/raw/train")
# dlFile(links['bioc'],outf['bioc'])
# unzipFile(outf['bioc'],outd['bioc'])
# biocCopy()