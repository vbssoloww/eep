from keras.models import Sequential
from keras.layers import LSTM, Dense, Flatten, Dropout
import numpy as np
import os, sys
import keras
from pprint import pprint
from glob import glob
from keras import backend as K


from keras.callbacks import Callback , CSVLogger
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score
import tensorflow as tf
from keras.backend.tensorflow_backend import set_session


config = tf.ConfigProto()
config.gpu_options.per_process_gpu_memory_fraction = 0.3
set_session(tf.Session(config=config))

batch_size = 100
timesteps = 10
data_dim = 201
num_classes = 58
lstm1 = 200
print (sys.argv)
lstm1 = int(sys.argv[1])
lstm2 = 200
lstm2 = int(sys.argv[2])
if len(sys.argv) == 0:
    lstm1 = lstm2 = 100
def f1(targ,pred):
    nt = targ.reshape(5000,58)
    npr = pred.reshape(5000,58)
    tfile = open('targ-'+str(lstm1)+'-'+str(lstm2)+'_pos.o','w')
    np.savetxt(tfile,nt,delimiter=',',fmt="%d")
    tfile.close()
    pfile = open('pred-'+str(lstm1)+'-'+str(lstm2)+'_pos.o','w')
    np.savetxt(pfile,npr,delimiter=',',fmt='%f')
    pfile.close()
    return
class Metrics(keras.callbacks.Callback):
    def on_epoch_end(self, batch, logs={}):
        predict = np.asarray(self.model.predict(self.validation_data[0],batch_size=batch_size))
        targ = self.validation_data[1]
        self.f1s=f1(targ, predict)
        return
metrics = Metrics()



            

# Load training data
x_train = np.loadtxt('./generated/sub/trainx_pos',delimiter=',')
y_train = np.loadtxt('./generated/sub/trainy',delimiter=',')
print(x_train.shape,y_train.shape)
n=x_train.shape[0]-62000
nn=y_train.shape[0]-62000
x_train = x_train[:-n, :]
y_train = y_train[:-nn, :]
print(x_train.shape,y_train.shape)
x_train = x_train.reshape((int)(x_train.shape[0]/timesteps),timesteps,data_dim)
y_train = y_train.reshape((int)(y_train.shape[0]/timesteps),timesteps,num_classes)
# Load validation data
x_val = np.loadtxt('./generated/sub/devx_pos',delimiter=',')
y_val = np.loadtxt('./generated/sub/devy',delimiter=',')
print(x_val.shape,y_val.shape)
n=x_val.shape[0]-5000
nn=y_val.shape[0]-5000
x_val = x_val[:-n, :]
y_val = y_val[:-nn, :]
print(x_val.shape,y_val.shape)
x_val = x_val.reshape((int)(x_val.shape[0]/timesteps),timesteps,data_dim)
y_val = y_val.reshape((int)(y_val.shape[0]/timesteps),timesteps,num_classes)
print(x_train.shape,y_train.shape)
print(x_val.shape,y_val.shape)


model = Sequential()
model.add(Dropout(0.2,batch_input_shape=(batch_size, timesteps, data_dim)))
model.add(LSTM(lstm1, return_sequences=True,
                stateful=True,
                # batch_input_shape=(batch_size, timesteps, data_dim),
                ))
# model.add(Dropout(0.2))
# model.add(Flatten())
# model.add(Dropout(0.2))
# model.add(Dropout(0.2))

# model.add(LSTM(32, stateful=True))
model.add(Dense(num_classes, activation='softmax'))

model.compile(loss='categorical_crossentropy',
          optimizer= "RMSprop",
          metrics=['categorical_accuracy'])
# for dir in os.listdir('.'):
    # print ( dir )

stopCallback = keras.callbacks.EarlyStopping(monitor='categorical_accuracy', min_delta=0.003, patience=20, verbose=0, mode='max')
csv_logger = CSVLogger(str(lstm1)+'-'+str(lstm2)+'-sub.csv')
model.fit(x_train, y_train,
        batch_size=batch_size,
        epochs=20,
        shuffle=False,
        validation_data=(x_val, y_val),
        callbacks=[stopCallback,csv_logger],
        verbose=1)

model.save('sub.h5')

pfile = open('pred.o','r')
tfile = open('targ.o','r')

# print(tc1,tc2,tc3)


