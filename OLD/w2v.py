import gensim, logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)


import os
from pprint import pprint

dev_path = './resources/ss/dev'
train_path = './resources/ss/train'
test_path = './resources/ss/test'
root_ss = './resources/ss'
dirs = os.listdir(root_ss)

sentences = []

def processFile(path):
    # print ( path )
    with open(path) as f:
        content = f.readlines()
    content = [x.strip(',').lower() for x in content]
    for x in content:
        tmp = x.translate({ord(c): None for c in '!@#$,:.[]'})
        tmp = tmp.split()
        new_list = []
        for i in range(len(tmp)):
            nw = tmp[i].strip('(').strip(')')
            # new_list.append(tmp[i])
            if nw != tmp[i]:
                new_list.append(nw)
            elif not nw.find('//') == -1:
                new_list=new_list+tmp[i].split('//')
            elif not nw.find('-') == -1:
                new_list=new_list+tmp[i].split('-')
            else:
                new_list.append(tmp[i])
        sentences.append(tmp)
        sentences.append(new_list)




for dir in dirs:
    dirs2 = os.listdir(os.path.join(root_ss,dir))
    for dir2 in dirs2:
        # print ( dir2 ) 
        processFile(os.path.join(root_ss,dir,dir2))
        # break

# pprint (sentences)
model = gensim.models.Word2Vec(sentences, window=5, min_count=1,size=200)
model.wv.save_word2vec_format('./model/self/w2v.bin', binary=True)
model.wv.save_word2vec_format('./model/self/w2v.model', binary=False)

pprint (model.most_similar('cancer'))
pprint (model.most_similar('UFT'))
pprint ( model['ODC'] )
pprint ( model['anchored'] )
pprint ( model['APO-1'] )


