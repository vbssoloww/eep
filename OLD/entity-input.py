import os
import gensim
from pprint import pprint
import numpy as np


num_entity_classes = 20
given_types = list([
        "Organism",
        "Organism_subdivision",
        "Anatomical_system",
        "Organ",
        "Multi-tissue_structure",
        "Tissue",
        "Developing_anatomical_structure",
        "Cell",
        "Cellular_component",
        "Organism_substance",
        "Immaterial_anatomical_entity",
        "Gene_or_gene_product",
        "Simple_chemical",
        "Amino_acid",
        "Pathological_formation",
        "Cancer",
        ])


more_types = [
        "Protein_domain_or_region",
        "DNA_domain_or_region",
        "Positive_regulation",
        "Negative_regulation",
        "Blood_vessel_development",
        "Planned_process",
        "Regulation",
        "Localization",
        "Growth",
        "Breakdown",
        "Cell_death",
        "Mutation",
        "Catabolism",
        "Death",
        "Cell_transformation",
        "Binding",
        "Gene_expression",
        "Metastasis",
        "Pathway",
        "Development",
        "Cell_division",
        "Cell_differentiation",
        "Cell_proliferation",
        "Translation",
        "Transcription",
        "Remodeling",
        "Carcinogenesis",
        "Metabolism",
        "Phosphorylation",
        "DNA_methylation",
        "Protein_processing",
        "Infection",
        "Synthesis",
        "Dissociation",
        "Glycolysis",
        "Dephosphorylation",
        "Ubiquitination",
        "Acetylation",
        "Amino_acid_catabolism",
        "Glycosylation",
        "DNA_demethylation"]
entity_types = given_types + more_types


print ( entity_types , type(given_types) )



dev_in_path = './resources/dev'
train_in_path = './resources/train'
test_in_path = './resources/test'

dev_entity_path = './generated/entity/dev'
train_entity_path = './generated/entity/train'
test_entity_path = './generated/entity/test'


def processFile(file_name,model,out_dir):
    ans_vectors = list()
    input_vectors = list()
    if file_name.endswith('.txt') or file_name.lower().endswith('license') or file_name.lower().endswith('readme') :
        return input_vectors, ans_vectors
    # print ( file_name )
    
    with open(file_name) as f:
        content = f.readlines()
        for annotation in content:
            words = annotation.lower().split()
            if not words[0][:1] == 't':
                continue
            # print ( words , len(words) )
            for i in range (4,len(words)):
                try:
                    input_vector = model[words[i].strip(')').strip('(')]
                    # print(type(input_vector),input_vector)
                    tans_vector = []
                    ok = False
                    for i in range (0,len(entity_types)):
                        if entity_types[i].lower() == words[1]:
                            tans_vector.append(1)
                            ok = True
                        else:
                            tans_vector.append(0)
                    # print (ans_vector)
                    if not ok:
                        print ( words[1] )
                    else:
                        ans_vector = np.array(tans_vector)
                        ans_vectors.append(ans_vector)
                        input_vectors.append(input_vector)
                except:
                    print ( "Key Error" )
    # print ( input_vectors , ans_vectors )
    return input_vectors, ans_vectors





def processDir(dir_path,dir_entity_path,model):
    dirs = os.listdir(dir_path)
    root = dir_path
    for dir in dirs:
        # print ( dir )
        # try:
        directory = dir_entity_path
        if not os.path.exists(directory):
            os.makedirs(directory)
        new_path = os.path.join(dir_entity_path,dir)
        
        inv, ansv = processFile(os.path.join(root,dir),model,new_path)
        if len(inv) == 0:
            continue
        input_file = open(new_path+'.in',"w")
        output_file = open(new_path+'.out',"w")
        # print (len(inv),len(ansv))
        if len(inv) == len(ansv) and len(inv) > 0:
            for i in range(len(inv)):
                np.savetxt(input_file,inv[i],delimiter=' ',newline=' ')
                input_file.write('\n')
                np.savetxt(output_file,ansv[i],delimiter=' ',newline=' ')
                output_file.write('\n')
                
        # except:
            # print ('Vocab Error')
        # break




def processAll():
    model = gensim.models.KeyedVectors.load_word2vec_format('./model/self/w2v.bin', binary=True)
    # model = gensim.models.KeyedVectors.load_word2vec_format('./resources/PubMed-w2v.bin', binary=True)
    processDir(dev_in_path,dev_entity_path,model)
    processDir(train_in_path,train_entity_path,model)
    processDir(test_in_path,test_entity_path,model)
    processFile('./resources/train/PMID-18505892.a1',model,'./generated/entity/train/PMID-18505892.a1')
    # processFile('./resources/train/PMID-18505892.a2',model,'./generated/entity/train/PMID-18505892.a2')




processAll()
# processDir(dev_in_path,dev_entity_path)
# model = gensim.models.KeyedVectors.load_word2vec_format('./resources/PubMed-w2v.bin', binary=True)


