from fpath import IN_DEV_PATH,IN_TRAIN_PATH,BIOC_POS_LEMMA
import os
from lxml import etree
from pprint import pprint
import gensim, logging

logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s', level=logging.INFO)
subbed = []
def wordAppend(word):
    subbed.append(word)
def substituePMID(root,apath,pmid,type_string):
    # print ( apath )
    a1 = pmid + '.a1'
    a2 = pmid + '.a2'
    fa1 = open(os.path.join(apath,a1),'r')
    fa2 = open(os.path.join(apath,a2),'r')
    a1_content = fa1.readlines()
    a2_content = fa2.readlines()
    fa1.close()
    fa2.close()

    # here
    # content processing

    off_dict = dict()

    for content in a1_content:
        words = content.split()
        if not words[0][0:1] == 'T':
            continue
        # print (type(words[1]))
        off_dict[(int)(words[2])] = words[1]


    for content in a2_content:
        words = content.split()
        if not words[0][0:1] == 'T':
            continue
        off_dict[(int)(words[2])] = words[1]

    # input vector section

    for sentence in root.iter('sentence'):
        new_list = []
        for anno in sentence.iter('annotation'):
            offset = (int)(anno[1].text)
            if offset in off_dict:
                new_list.append(off_dict[offset].lower())
            else:
                word = anno[3].text.lower()
                new_list.append(word)
                # if word == 'cancer':
                    # print ('cancer found!')
        wordAppend(new_list)
    return
def substituteAll():
    tree = etree.parse(BIOC_POS_LEMMA)
    root = tree.getroot()
    for child in root.iter('document'):
        id = child.find('id')
        pmid = id.text
        pmid = pmid.split('.')[0]
        
        # dev
        ta = str(pmid) + ".a1"
        tpath = os.path.join(IN_DEV_PATH,ta)
        if os.path.exists(tpath):
            # go dev
            substituePMID(child,IN_DEV_PATH,pmid,"dev")
        # train
        ta = str(pmid) + ".a1"
        tpath = os.path.join(IN_TRAIN_PATH,ta)
        if os.path.exists(tpath):
            # go train
            substituePMID(child,IN_TRAIN_PATH,pmid,"train")
    



substituteAll()
print (subbed)
print ('cancer' in subbed)
model = gensim.models.Word2Vec(subbed, window=5, min_count=1,size=200)
model.wv.save_word2vec_format('./model/self/w2vsub.bin', binary=True)
model.wv.save_word2vec_format('./model/self/w2vsub.model', binary=False)

pprint (model.most_similar('cancer'))
# pprint (model.most_similar('UFT'))
pprint ( model['positive_regulation'] )
# pprint ( model['ODC'] )
# pprint ( model['anchored'] )
# pprint ( model['APO-1'] )