from keras.models import Sequential
from keras.layers import LSTM, Dense, Flatten
import numpy as np
import os
import keras
from pprint import pprint
from glob import glob
from keras import backend as K
from fpath import trainfile_x_path,trainfile_y_path,valfile_x_path,valfile_y_path


from keras.callbacks import Callback , CSVLogger
from sklearn.metrics import confusion_matrix, f1_score, precision_score, recall_score



batch_size = 50
timesteps = 2
data_dim = 200
num_classes = 57
lstm1 = 333
lstm2 = 333
model = Sequential()
model.add(LSTM(lstm1, return_sequences=True,
                stateful=True,
                batch_input_shape=(batch_size, timesteps, data_dim),
                ))
# model.add(Flatten())

# model.add(LSTM(32, stateful=True))
model.add(Dense(57, activation='softmax'))

model.compile(loss='categorical_crossentropy',
          optimizer= "RMSprop",
          metrics=['categorical_accuracy'])
# for dir in os.listdir('.'):
    # print ( dir )



            

# Load training data
x_train = np.load(trainfile_x_path)
y_train = np.load(trainfile_y_path)
n=x_train.shape[0]-20000
nn=y_train.shape[0]-20000
x_train = x_train[:-n, :]
y_train = y_train[:-nn, :]
x_train = x_train.reshape((int)(x_train.shape[0]/timesteps),timesteps,data_dim)
y_train = y_train.reshape((int)(y_train.shape[0]/timesteps),timesteps,num_classes)
# Load validation data
x_val = np.load(valfile_x_path)
y_val = np.load(valfile_y_path)
n=x_val.shape[0]-7000
nn=y_val.shape[0]-7000
x_val = x_val[:-n, :]
y_val = y_val[:-nn, :]
x_val = x_val.reshape((int)(x_val.shape[0]/timesteps),timesteps,data_dim)
y_val = y_val.reshape((int)(y_val.shape[0]/timesteps),timesteps,num_classes)
print(x_train.shape,y_train.shape)
print(x_val.shape,y_val.shape)

stopCallback = keras.callbacks.EarlyStopping(monitor='categorical_accuracy', min_delta=0.005, patience=10, verbose=0, mode='max')
csv_logger = CSVLogger(str(lstm1)+'-'+str(lstm2)+'.csv')
model.fit(x_train, y_train,
        batch_size=batch_size,
        epochs=4000,
        shuffle=False,
        validation_data=(x_val, y_val),
        callbacks=[stopCallback,csv_logger],
        verbose=1)

# print(tc1,tc2,tc3)


