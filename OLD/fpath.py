IN_DEV_PATH = './resources/dev'
IN_TRAIN_PATH = './resources/train'
IN_TEST_PATH = './resources/test'

TOK_DEV_PATH = './resources/tok/dev'
TOK_TRAIN_PATH = './resources/tok/train'
TOK_TEST_PATH = './resources/tok/test'

SS_DEV_PATH = './resources/tok/dev'
SS_TRAIN_PATH = './resources/tok/train'
SS_TEST_PATH = './resources/tok/test'


SEQ_DEV_PATH = './generated/seq/dev'
SEQ_TRAIN_PATH = '.resources/seq/train'
SEQ_TEST_PATH = '.resources/seq/test'

DEV_ENTITY_PATH = './generated/entity/dev'
TRAIN_ENTITY_PATH = '.resources/entity/train'
TEST_ENTITY_PATH = '.resources/entity/test'

TRAINFILE_X_PATH = './generated/input/trainx.npy'
TRAINFILE_Y_PATH = './generated/input/trainy.npy'

VALFILE_X_PATH = './generated/input/valx.npy'
VALFILE_Y_PATH = './generated/input/valy.npy'

BIOC_POS_LEMMA = './resources/bioc/data/cg_pos_lemma.xml'
BIOC_SENTENCE = './resources/bioc/data/cg_sentence.xml'
BIOC_TEXT = './resources/bioc/data/cg_text.xml'
