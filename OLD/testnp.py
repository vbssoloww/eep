import numpy as np
batch_size = 50
timesteps = 2
data_dim = 200
num_classes = 58
lstm1 = 333
lstm2 = 333
# Load training data
x_train = np.loadtxt('./generated/seq/trainx',delimiter=',')
y_train = np.loadtxt('./generated/seq/trainy',delimiter=',')
n=x_train.shape[0]-68000
nn=y_train.shape[0]-68000
x_train = x_train[:-n, :]
y_train = y_train[:-nn, :]
print(x_train.shape,y_train.shape)
x_train = x_train.reshape((int)(x_train.shape[0]/timesteps),timesteps,data_dim)
y_train = y_train.reshape((int)(y_train.shape[0]/timesteps),timesteps,num_classes)
# Load validation data
x_val = np.loadtxt('./generated/seq/devx',delimiter=',')
y_val = np.loadtxt('./generated/seq/devy',delimiter=',')
n=x_val.shape[0]-22000
nn=y_val.shape[0]-22000
x_val = x_val[:-n, :]
y_val = y_val[:-nn, :]
print(x_val.shape,y_val.shape)
x_val = x_val.reshape((int)(x_val.shape[0]/timesteps),timesteps,data_dim)
y_val = y_val.reshape((int)(y_val.shape[0]/timesteps),timesteps,num_classes)
print(x_train.shape,y_train.shape)
print(x_val.shape,y_val.shape)