import os
from pprint import pprint
from fpath import BIOC_POS_LEMMA, IN_DEV_PATH, IN_TRAIN_PATH
from lxml import etree
from pprint import pprint
import gensim
import numpy as np
num_entity_classes = 20
given_types = list([
        "Organism",
        "Organism_subdivision",
        "Anatomical_system",
        "Organ",
        "Multi-tissue_structure",
        "Tissue",
        "Developing_anatomical_structure",
        "Cell",
        "Cellular_component",
        "Organism_substance",
        "Immaterial_anatomical_entity",
        "Gene_or_gene_product",
        "Simple_chemical",
        "Amino_acid",
        "Pathological_formation",
        "Cancer",
        ])


more_types = [
        "Protein_domain_or_region",
        "DNA_domain_or_region",
        "Positive_regulation",
        "Negative_regulation",
        "Blood_vessel_development",
        "Planned_process",
        "Regulation",
        "Localization",
        "Growth",
        "Breakdown",
        "Cell_death",
        "Mutation",
        "Catabolism",
        "Death",
        "Cell_transformation",
        "Binding",
        "Gene_expression",
        "Metastasis",
        "Pathway",
        "Development",
        "Cell_division",
        "Cell_differentiation",
        "Cell_proliferation",
        "Translation",
        "Transcription",
        "Remodeling",
        "Carcinogenesis",
        "Metabolism",
        "Phosphorylation",
        "DNA_methylation",
        "Protein_processing",
        "Infection",
        "Synthesis",
        "Dissociation",
        "Glycolysis",
        "Dephosphorylation",
        "Ubiquitination",
        "Acetylation",
        "Amino_acid_catabolism",
        "Glycosylation",
        "DNA_demethylation"]

none_type  = ["None"]
entity_types = given_types + more_types + none_type
modelself = gensim.models.KeyedVectors.load_word2vec_format('./model/self/w2vsub.bin', binary=True)
# model = gensim.models.KeyedVectors.load_word2vec_format('./resources/PubMed-w2v.bin', binary=True)

def processPMID(a1_path,a2_path,root,type_string):
    fa1 = open(a1_path,'r')
    fa2 = open(a2_path,'r')

    a1_content = fa1.readlines()
    a2_content = fa2.readlines()

    fa1.close()
    fa2.close()

    # content processing

    a1_array = []
    a2_array = []
    for content in a1_content:
        words = content.split()
        if not words[0][0:1] == 'T':
            continue
        tmp1 = dict()
        tmp1['offset1'] = words[2]
        tmp1['offset2'] = words[3]
        tmp1['type'] = words[1]
        a1_array.append(tmp1)

    # pprint(a1_array)

    for content in a2_content:
        words = content.split()
        if not words[0][0:1] == 'T':
            continue
        tmp2 = dict()
        tmp2['offset1'] = words[2]
        tmp2['offset2'] = words[3]
        tmp2['type'] = words[1]
        a2_array.append(tmp2)

    # pprint(a2_array)
    # input vector section

    # print(len(a1_array),len(a2_array))

    pos_dict = dict()
    pos_cnt = 0
    for anno in root.iter('annotation'):
        postype = anno[0]
        offset = anno[1]
        len = anno[2]
        text = anno[3]
        postype = (postype.text).split('|')[0]
        if not postype in pos_dict:
            pos_dict[postype] = pos_cnt
            pos_cnt = pos_cnt + 1
        bad_words = ['.',',',':',';','[',']','%','"',"'","?"]
        if text.text in bad_words:
            continue

        off1 = (int)(offset.text)
        off2 = (int)(offset.text) + (int)(len.text)
        # print(off1,off2)

        etype = "None"
        for a1_anno in a1_array:
            # a1
            if off1 >= (int)(a1_anno['offset1']) and off2 <= (int)(a1_anno['offset2']):
                etype = a1_anno['type']
                break

        for a2_anno in a2_array:
            # a2
            if off1 >= (int)(a2_anno['offset1']) and off2 <= (int)(a2_anno['offset2']):
                etype = a1_anno['type']
                break
        
        output_vector = []
        for type_iter in entity_types:
            if etype == type_iter:
                output_vector.append(1)
            else:
                output_vector.append(0)
        try:
            # if text.text.lower() in model.wv.vocab:
                # input_vector = model[text.text.lower()]
            if text.text.lower() in modelself.wv.vocab:
                input_vector = modelself[text.text.lower()]
                pos_vector = []
                for i in range(input_vector.shape[0]):
                    pos_vector.append(input_vector[i])
                pos_vector.append(pos_dict[postype])
                # print (type(pos_vector))
                # print (type(output_vector))
                # pos_vector = input_vector.append(pos_dict[postype])
                pref_path = './generated/sub/'
                in_handler = open (pref_path + type_string + 'x', "a")
                out_handler = open (pref_path + type_string+ 'y' , "a")
                pos_handler = open (pref_path + type_string + 'x_pos' ,"a")

                tnp = np.array(input_vector)
                np.savetxt(in_handler,tnp[np.newaxis],delimiter=',')
                in_handler.close()

                tnp = np.array(output_vector)
                np.savetxt(out_handler,tnp[np.newaxis],delimiter=',',fmt='%d')
                out_handler.close()

                tnp = np.array(pos_vector)
                np.savetxt(pos_handler,tnp[np.newaxis],delimiter=',')
                pos_handler.close()
            else:
                print ('no key' , text.text)
                # text.text 
                # print ('fail pos')
        except:
            continue
            # print ( text.text )
            # print('failpos')
    # print ( nokeycnt )
    return

def processXML(xml_path):
    tree = etree.parse(xml_path)
    root = tree.getroot()
    # etree.tostring(root)
    # print(len(root))
    devcnt = 0
    traincnt = 0
    for child in root.iter("document"):

        id = child.find('id')
        pmid = id.text
        pmid = pmid.split('.')[0]
        
        # print ('pmid',pmid)
        a1 = pmid + '.a1'
        a2 = pmid + '.a2'
        
        
        
        a1_tmp_path = IN_DEV_PATH + '/' +a1
        a2_tmp_path = IN_DEV_PATH + '/' +a2
        # print (a1_tmp_path,a2_tmp_path)
        

        #   dev
        if os.path.exists(a1_tmp_path):
            devcnt = devcnt+1
            processPMID (a1_tmp_path,a2_tmp_path,child,'dev')
                
        

        a1_tmp_path = IN_TRAIN_PATH + '/' +a1
        a2_tmp_path = IN_TRAIN_PATH + '/' +a2
        # print (a1_tmp_path,a2_tmp_path)
        
        #   train 
        if os.path.exists(a1_tmp_path):
            traincnt = traincnt+1
            processPMID (a1_tmp_path,a2_tmp_path,child,'train')
    print (devcnt,traincnt)

def processAll():
    # print ('111')
    processXML(BIOC_POS_LEMMA)
    return

processAll()
