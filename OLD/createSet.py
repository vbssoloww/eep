# from keras.models import Sequential
# from keras.layers import LSTM, Dense
import numpy as np
import os
from pprint import pprint
from glob import glob


dev_path = './generated/entity/dev'
train_path = './generated/entity/train'
test_path = './generated/entity/test'

print(dev_path)

# Expected input batch shape: (batch_size, timesteps, data_dim)
# Note that we have to provide the full batch_input_shape since the network is stateful.
# the sample of index i in batch k is the follow-up for the sample i in batch k-1.
# model = Sequential()
# model.add(LSTM(32, return_sequences=True, stateful=True,
#                batch_input_shape=(batch_size, timesteps, data_dim)))
# model.add(LSTM(32, return_sequences=True, stateful=True))
# model.add(LSTM(32, stateful=True))
# model.add(Dense(10, activation='softmax'))

# model.compile(loss='categorical_crossentropy',
#               optimizer='rmsprop',
#               metrics=['accuracy'])
# for dir in os.listdir('.'):
    # print ( dir )
def getData(path):
    print (path)
    dirs = os.listdir(path)
    # a = np.array([[1, 2], [3, 4]])
    # b = np.array([[5, 6]])
    # print (a.shape,b.shape)
    # return [],[]
    invs = list()
    outvs = list()
    last_dir = ''
    for dir in dirs:
        file_path = os.path.join(path,dir)
        # print(os.path.splitext(dir)[0])
        # print(os.path.splitext(last_dir)[0])
        name1 = os.path.splitext(dir)[0]
        name2 = os.path.splitext(last_dir)[0]
        # print(name1,name2)
        
        if not name1 == name2:
            last_dir = dir
            continue
        if dir == 'LICENSE.in' or dir == 'README.in' or dir == 'LICENSE.out' or dir == 'README.out':
            last_dir = dir
            continue
        file_path = os.path.join(path,dir)
        ffile_path = os.path.join(path,last_dir)
        last_dir = dir
        f = open(file_path,"r")
        ff = open(ffile_path,"r")
        try:
            tout = np.genfromtxt(f, dtype=None, delimiter=' ',encoding='str')
            tin = np.genfromtxt(ff, dtype=None, delimiter=' ',encoding='str')
            # print (type(tin) , tin.shape ,type(tout) , tout.shape)
            if tin.shape[1] == 200 and tout.shape[1] == 57:
                invs.append(tin)
                outvs.append(tout)
        except:
            # print (file_path)
            print ('to be debugged')
        

    input_array = np.vstack(invs)
    output_array = np.vstack(outvs)
    
    return input_array,output_array



            

# Generate training data
trainfile_x ='./generated/input/trainx.npy'
trainfile_y ='./generated/input/trainy.npy'
x_train, y_train = getData(train_path)
# getData(train_path)
np.save(trainfile_x,x_train)
np.save(trainfile_y,y_train)
print(x_train.shape,y_train.shape)
# Generate validation data
valfile_x ='./generated/input/valx.npy'
valfile_y ='./generated/input/valy.npy'
x_val, y_val = getData(dev_path)
np.save(valfile_x,x_val)
np.save(valfile_y,y_val)
print(x_val.shape,y_val.shape)


# model.fit(x_train, y_train,
        #   batch_size=batch_size, epochs=100, shuffle=False,
        #   validation_data=(x_val, y_val))


